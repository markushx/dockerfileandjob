FROM alpine:3.5

ENV MKDOCS_VERSION="1.0.4"

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates

RUN \
    apk add --update \
        bash \
        git \
        openssh \
        python2 \
        python2-dev \
        py-setuptools; \
    easy_install-2.7 pip && \
    pip install mkdocs==${MKDOCS_VERSION} && \
    rm -rf /tmp/* /var/tmp/* /var/cache/apk/* /var/cache/distfiles/*

WORKDIR /usr/local/bin

# Change `app` to whatever your binary is called

CMD ["./app"]

# test change in dockerfile again once more